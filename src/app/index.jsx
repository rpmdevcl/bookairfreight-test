import "./main.css";

import { QuotesContextProvider } from "../ui/contexts/quotes";

import NewQuote from "../ui/forms/NewQuote";
import Quotes from "../ui/layouts/Quotes";

function APP() {
  return (
    <QuotesContextProvider>
      <div className="app-container" data-testid="app">
        <NewQuote />
        <Quotes />
      </div>
    </QuotesContextProvider>
  );
}

export default APP;
