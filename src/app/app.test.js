import { render, screen } from "@testing-library/react";
import App from ".";

describe("APP", () => {
  test("renders new quote form", () => {
    render(<App />);
    expect(screen.getByTestId("new-quote-form")).toBeInTheDocument();
  });
});
