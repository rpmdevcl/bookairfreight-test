import React, { useContext } from "react";

import { QuotesContext } from "../contexts/quotes";

function Quotes() {
  const { quotes } = useContext(QuotesContext);
  const formatDate = (days = 0) => {
    const today = new Date();
    return new Date(today.setDate(today.getDate() + days))
      .toDateString()
      .split(" ")
      .slice(1, -1)
      .join(" ");
  };
  return (
    <div style={{ display: "flex", flex: 1, flexDirection: "column" }}>
      {quotes.map(
        (
          {
            countryFrom,
            countryDestination,
            quotePrice,
            shippingChannel,
            delivery = [],
          },
          i
        ) => (
          <div key={i} style={{ display: "flex" }}>
            <div style={{ width: "30%" }}>
              <div
                className="bordered"
                style={{ display: "flex", alignItems: "center" }}
              >
                <i
                  className={`icon fas fa-${
                    shippingChannel === "air" ? "plane" : "ship"
                  } fa-2x`}
                ></i>
                <p>{`Traditional ${shippingChannel} freight.`}</p>
              </div>
              <div className="bordered" style={{ padding: 10 }}>
                <p className="delivery-days">{`${delivery.join("-")} days`}</p>
                <p
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    margin: 0,
                    paddingTop: 10,
                  }}
                >
                  Estimated delivery
                </p>
                <p
                  style={{
                    textAlign: "center",
                    fontSize: 15,
                    paddingTop: 10,
                  }}
                >
                  <b>{`${formatDate(delivery[0])} - ${formatDate(
                    delivery[1]
                  )}`}</b>
                </p>
              </div>
            </div>
            <div className="bordered" style={{ flex: 1 }}>
              <div style={{ height: "20%", display: "flex" }}>
                <p className="route">
                  {`${countryFrom} -> ${countryDestination}`}
                </p>
              </div>
              <div
                style={{
                  height: "80%",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <p
                  style={{ margin: 0, textAlign: "center", fontSize: 40 }}
                >{`US ${new Intl.NumberFormat("en-US", {
                  style: "currency",
                  currency: "USD",
                  minimumFractionDigits: 0,
                  maximumFractionDigits: 0,
                  useGrouping: true,
                }).format(quotePrice)}`}</p>
              </div>
            </div>
          </div>
        )
      )}
    </div>
  );
}

export default Quotes;
