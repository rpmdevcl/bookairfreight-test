import React, { createContext, useReducer } from "react";

export const QuotesContext = createContext();

const actions = {
  NEW_QUOTE: "new_quote",
};

const reducer = (state, action) => {
  switch (action.type) {
    case actions.NEW_QUOTE:
      return {
        ...state,
        quotes: [action.quote],
      };
    default:
      return state;
  }
};

export const QuotesContextConsumer = QuotesContext.Consumer;

export const QuotesContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, {
    quotes: [],
  });
  const handleNewQuote = (quote) =>
    new Promise((resolve) =>
      resolve(
        dispatch({
          type: actions.NEW_QUOTE,
          quote,
        })
      )
    );
  return (
    <QuotesContext.Provider
      value={{
        ...state,
        handleNewQuote,
      }}
    >
      {props.children}
    </QuotesContext.Provider>
  );
};
