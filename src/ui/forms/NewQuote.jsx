import React, { useContext, useEffect, useState } from "react";

import { QuotesContext } from "../contexts/quotes";

function NewQuote() {
  const { handleNewQuote } = useContext(QuotesContext);
  const [countries, setCountries] = useState([]);
  const [loading, toggleLoading] = useState(true);
  const [values, setValues] = useState({
    countryFrom: "",
    countryDestination: "",
    quotePrice: 0,
    shippingChannel: "",
  });
  useEffect(() => {
    const fetchCountries = async () =>
      await fetch("https://restcountries.eu/rest/v2/all")
        .then((response) => response.json())
        .then((data) => setCountries(data))
        .finally(toggleLoading((prev) => !prev));
    fetchCountries().catch((error) => console.log(error));
  }, []);
  const estimateDelivery = () => {
    let bottomRange = null,
      topRange = null;
    switch (shippingChannel) {
      case "air":
        bottomRange = Math.floor(Math.random() * (7 - 3 + 1)) + 3;
        topRange = bottomRange + Math.floor(Math.random() * (4 - 2 + 1)) + 2;
        return [bottomRange, topRange];
      case "ocean":
        bottomRange = Math.floor(Math.random() * (30 - 25 + 1)) + 25;
        topRange = bottomRange + Math.floor(Math.random() * (10 - 5 + 1)) + 5;
        return [bottomRange, topRange];
      default:
        break;
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    handleNewQuote({ ...values, delivery: estimateDelivery() });
  };
  const handleChange = (event) =>
    setValues((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  const { countryFrom, countryDestination, quotePrice, shippingChannel } =
    values;
  if (loading) return <h1>Loading ...</h1>;
  return (
    <form
      name="new-quote-form"
      data-testid="new-quote-form"
      onSubmit={handleSubmit}
      style={{ width: 250 }}
    >
      <fieldset>
        <label>Starting country</label>
        <select
          id="countryFrom"
          name="countryFrom"
          value={countryFrom}
          data-testid="country-from"
          onChange={handleChange}
          required
        >
          <option disabled value="" defaultValue>
            Select Country
          </option>
          {countries?.map(({ alpha2Code, name }) => (
            <option key={alpha2Code} value={name}>
              {name}
            </option>
          ))}
        </select>
      </fieldset>
      <fieldset>
        <label>Destination country</label>
        <select
          id="countryDestination"
          name="countryDestination"
          value={countryDestination}
          data-testid="country-destination"
          onChange={handleChange}
          required
        >
          <option disabled value="" defaultValue>
            Select Country
          </option>
          {countries?.map(({ alpha2Code, name }) => (
            <option key={alpha2Code} value={name}>
              {name}
            </option>
          ))}
        </select>
      </fieldset>
      <fieldset>
        <label>Quote price</label>
        <input
          id="quote-price"
          name="quotePrice"
          value={quotePrice}
          type="number"
          data-testid="quote-price"
          onChange={handleChange}
          required
        />
      </fieldset>
      <fieldset>
        <label>Shipping channel</label>
        <select
          id="shipping-channel"
          name="shippingChannel"
          value={shippingChannel}
          data-testid="shipping-channel"
          onChange={handleChange}
          required
        >
          <option disabled value="" defaultValue>
            Select Channel
          </option>
          <option value="air">Air</option>
          <option value="ocean">Ocean</option>
        </select>
      </fieldset>
      <button className="btn-submit" type="submit">
        Create quote
      </button>
    </form>
  );
}

export default NewQuote;
