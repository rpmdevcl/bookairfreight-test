import { act, render, screen } from "@testing-library/react";

import Form from "./NewQuote";

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        name: "Country Name",
      }),
  })
);

describe("New Quote", () => {
  it("fetch countries on mount", async () => {
    await act(async () => render(<Form />));
    expect(screen.getByTestId("new-quote-form")).toBeInTheDocument();
  });
});
